package com.o4oxide.ore;

public class OreEvent<T> {
    private final String type;
    private final T payload;
    private final long timeStamp = System.nanoTime();

    public OreEvent(String type, T payload) {
        this.type = type;
        this.payload = payload;
    }

    public String getType() {
        return type;
    }

    public T getPayload() {
        return payload;
    }

    public long getTimeStamp() {
        return timeStamp;
    }
}
