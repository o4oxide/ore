package com.o4oxide.ore;


import com.o4oxide.ore.backend.dbms.rocksprotobuf.RocksProtobufDBMS;
import com.o4oxide.ore.core.processors.SQLSchemaProcessor;
import com.o4oxide.ore.exceptions.OreRuntimeException;
import com.o4oxide.ore.utils.snowflake.Snowflake;
import com.o4oxide.ore.utils.snowflake.Creator;
import com.o4oxide.syncreactive.SyncReactive;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Flow;
import java.util.concurrent.ForkJoinPool;

final class OreImpl implements Ore {

    private static Creator<ForkJoinPool, OreImpl> initializer = new Creator<>(() -> new OreImpl(null));

    private final Snowflake snowflake;
    private final SyncReactive syncReactive;
    private final SQLSchemaProcessor schemaProcessor;

    static Ore create() {
        return initializer.singleton();
    }

    static Ore create(ForkJoinPool pool) {
        return initializer.singleton(pool);
    }

    private OreImpl(ForkJoinPool pool) {
        syncReactive = pool == null ? SyncReactive.create(pool) : SyncReactive.create();
        snowflake = Snowflake.create();
        schemaProcessor = SQLSchemaProcessor.create(new RocksProtobufDBMS());
    }

    @Override
    public long generateUniqueKey() {
        return snowflake.nextId();
    }

    @Override
    public CompletableFuture<Integer> createTopic(String sql) {
        try {
            return syncReactive.async(schemaProcessor::process, sql);
        } catch (OreRuntimeException ex) {
            return CompletableFuture.failedFuture(ex);
        }
    }

    @Override
    public CompletableFuture<Integer> createMaterializedView(String sql) {
        try {
            return syncReactive.async(schemaProcessor::process, sql);
        } catch (OreRuntimeException ex) {
            return CompletableFuture.failedFuture(ex);
        }
    }

    @Override
    public CompletableFuture<Integer> logEvent(String sql) {
        return null;
    }

    @Override
    public <T> Flow.Subscription query(String query, Flow.Subscriber<T> subscriber) {
        return null;
    }
}
