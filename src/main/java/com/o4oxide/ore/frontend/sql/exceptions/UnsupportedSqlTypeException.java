package com.o4oxide.ore.frontend.sql.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class UnsupportedSqlTypeException extends OreRuntimeException {
    public UnsupportedSqlTypeException(String message) {
        super(message);
    }
}
