package com.o4oxide.ore.frontend.sql;

public class ColumnSpecs {

    private boolean nullable = true;
    private String defaultValue;

    public boolean isNullable() {
        return nullable;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
}
