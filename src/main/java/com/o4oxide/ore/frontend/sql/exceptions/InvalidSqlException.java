package com.o4oxide.ore.frontend.sql.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class InvalidSqlException extends OreRuntimeException {
    public InvalidSqlException(String message) {
        super(message);
    }
}
