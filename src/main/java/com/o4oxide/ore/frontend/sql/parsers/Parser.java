package com.o4oxide.ore.frontend.sql.parsers;

public interface Parser<T> {
    T parse(String sql);
}
