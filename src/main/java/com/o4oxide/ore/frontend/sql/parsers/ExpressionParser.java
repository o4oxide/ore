package com.o4oxide.ore.frontend.sql.parsers;

import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.schema.Column;

public class ExpressionParser extends ExpressionVisitorAdapter implements Parser {

    @Override
    public Object parse(String sql) {
        return null;
    }

    @Override
    public void visit(Column column) {
        super.visit(column);
    }

    @Override
    public void visit(LongValue value) {
        super.visit(value);

    }
}
