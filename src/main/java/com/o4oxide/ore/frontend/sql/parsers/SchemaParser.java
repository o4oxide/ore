package com.o4oxide.ore.frontend.sql.parsers;

import com.o4oxide.ore.core.models.schema.BoundableFieldSchema;
import com.o4oxide.ore.core.models.schema.EnumSchema;
import com.o4oxide.ore.core.models.schema.FieldSchema;
import com.o4oxide.ore.core.models.schema.RecordSchema;
import com.o4oxide.ore.frontend.sql.exceptions.InvalidSqlException;
import com.o4oxide.ore.frontend.sql.exceptions.SQLParseException;
import com.o4oxide.ore.frontend.sql.exceptions.UnsupportedSqlException;
import com.o4oxide.ore.frontend.sql.ColumnSpecs;
import com.o4oxide.ore.frontend.sql.exceptions.UnsupportedSqlTypeException;
import com.o4oxide.ore.utils.Checkers;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.*;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.comment.Comment;
import net.sf.jsqlparser.statement.create.index.CreateIndex;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.view.AlterView;
import net.sf.jsqlparser.statement.create.view.CreateView;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.execute.Execute;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.merge.Merge;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.statement.upsert.Upsert;
import net.sf.jsqlparser.statement.values.ValuesStatement;

import java.util.*;

import static com.o4oxide.ore.core.models.schema.FieldSchema.Type;

/**
 * Parses SQL schema statements, specifically DECLARE and CREATE
 *
 * DECLARE - Define a record schema that is used as a variable in another record
 *  only DECLARE TABLE is supported. DECLARE AS is not supported.
 *  It is used in situations where nested records are desired.
 *  The supported use case is a sequence of DECLARE TABLE statements which will define the records to be embedded in
 *  a CREATE TABLE record.
 *  A DECLARE statement definition can contain a reference to another record declared earlier.
 *  This implies that a record that is to be nested in another record must already have been defined
 *  with a DECLARE statement.
 *  In other to limit the complexity of the CREATE TABLE record, we limit nesting depth to 3
 *  and number of declared variables to 7 (both values will be reviewed in the future)
 *
 *  CREATE - Define a record schema that is NOT used as a variable in another record
 *  This must be the last statement and all record variables must already have been defined in DECLARE
 *
 */
public class SchemaParser implements Parser<RecordSchema>, StatementVisitor {

    private boolean schemaDefinitionEnded;

    private RecordSchema currentRecord;
    private final Map<RecordSchema, Boolean> declaredVariablesToUsage = new HashMap<>();

    private final ColumnSpecsParser columnSpecsParser = new ColumnSpecsParser();

    public RecordSchema parse(String sql) {
        try {
            Statements statements = CCJSqlParserUtil.parseStatements(sql);
            statements.accept(this);
            return currentRecord;
        } catch (JSQLParserException ex) {
            throw new SQLParseException(ex);
        }
    }

    @Override
    public void visit(DeclareStatement declareStatement) {
        if (DeclareType.TABLE != declareStatement.getType()) {
            throw new UnsupportedSqlException("DECLARE is only allowed on TABLE type");
        }
        initializeRecord(declareStatement.getUserVariable().getName());
        declaredVariablesToUsage.put(currentRecord, false);
        extractFieldSchema(declareStatement.getColumnDefinitions());
    }


    @Override
    public void visit(CreateTable createTable) {
        initializeRecord(createTable.getTable().getName());
        extractFieldSchema(createTable.getColumnDefinitions());
        schemaDefinitionEnded = true;
        validateAllVariablesWereUsed();
    }

    private void initializeRecord(String recordName) {
        if (schemaDefinitionEnded) {
            throw new InvalidSqlException("Schema definition should end with single CREATE statement");
        }
        currentRecord = new RecordSchema(recordName);
    }

    private void extractFieldSchema(List<ColumnDefinition> columnDefinitions) {
        columnDefinitions.forEach(columnDefinition -> {
            String fieldName = columnDefinition.getColumnName().toLowerCase();
            Type fieldType = convertSqlType(columnDefinition.getColDataType().getDataType());
            validateColDataType(fieldName, fieldType, columnDefinition.getColDataType());
            FieldSchema field = createField(fieldName, fieldType, columnDefinition.getColDataType());
            setOptions(field, columnDefinition.getColumnSpecStrings());
            currentRecord.addField(field);
            if (Type.RECORD == fieldType) {
                declaredVariablesToUsage.put((RecordSchema) field, true);
            }
        });
    }

    private void validateColDataType(String name, Type type, ColDataType colDataType) {
        if (Type.ENUM == type && Checkers.isEmpty(colDataType.getArgumentsStringList())) {
            throw new InvalidSqlException("Enum type requires value specification (" + name + ")");
        }
        if (Type.ENUM != type && !type.isBoundable() && Checkers.isNotEmpty(colDataType.getArgumentsStringList())) {
            throw new InvalidSqlException("Field type is not boundable (" + name + ")");
        }
    }

    private FieldSchema createField(String fieldName, Type fieldType, ColDataType colDataType) {
        FieldSchema field;
        if (Type.RECORD == fieldType) {
            field = getRecord(fieldName);
        } else if (Type.ENUM == fieldType) {
            field = new EnumSchema(fieldName, colDataType.getArgumentsStringList());
        } else if (fieldType.isBoundable() && Checkers.isNotEmpty(colDataType.getArgumentsStringList())) {
            try {
                int[] bounds = getBounds(colDataType.getArgumentsStringList());
                field = new BoundableFieldSchema(fieldName, fieldType, bounds);
            } catch (NumberFormatException ex) {
                throw new InvalidSqlException("Boundable type argument must be integer (" + fieldName + ")");
            }
        } else {
            field = new FieldSchema(fieldName, fieldType);
        }

        List<Integer> arrayData = colDataType.getArrayData();
        if (Checkers.isNotEmpty(arrayData)) {
            int arraySize = arrayData.get(0) == null ? Integer.MAX_VALUE : arrayData.get(0);
            field.setArraySize(arraySize);
        }

        return field;
    }

    private RecordSchema getRecord(String name) {
        return declaredVariablesToUsage.keySet().stream()
                .filter(rec -> rec.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new InvalidSqlException("Cannot find declaration for record variable " + name));
    }

    private int[] getBounds(List<String> arguments) {
        int[] bounds = new int[arguments.size()];
        for (int i = 0; i < arguments.size(); i++) {
            bounds[i] = Integer.parseInt(arguments.get(i));
        }
        return bounds;
    }

    private void setOptions(FieldSchema field, List<String> columnSpecsArguments) {
        ColumnSpecs columnSpecs = columnSpecsParser.parse(columnSpecsArguments);
        if (columnSpecs != null) {
            field.setNullable(columnSpecs.isNullable());
            field.setDefaultValue(columnSpecs.getDefaultValue());
        }
    }

    public static Type convertSqlType(String sqlType) {
        switch (sqlType) {
            case "INT":
            case "INTEGER":
                return Type.INTEGER;
            case "BIT":
            case "BOOL":
            case "BOOLEAN":
                return Type.BOOLEAN;
            case "BIGINT":
            case "INT8":
                return Type.LONG;
            case "FLOAT":
            case "REAL":
                return Type.FLOAT;
            case "DOUBLE":
            case "FLOAT8":
                return Type.DOUBLE;
            case "BYTEA":
            case "RAW":
            case "BINARY":
                return Type.BYTES;
            case "VARCHAR":
            case "NVARCHAR":
            case "CHAR":
            case "NCHAR":
            case "CHARACTER":
                return Type.STRING;
            case "RECORD":
                return Type.RECORD;
            case "ENUM":
                return Type.ENUM;
        }
        throw new UnsupportedSqlTypeException(sqlType + " is not supported");
    }


    private void validateAllVariablesWereUsed() {
        if (declaredVariablesToUsage.values().stream().anyMatch(usage -> !usage)) {
            throw new InvalidSqlException("Some declared record variables were not used");
        }
    }

    @Override
    public void visit(Select select) {
        throw new UnsupportedSqlException("Select statement not supported");
    }

    @Override
    public void visit(Upsert upsert) {
        throw new UnsupportedSqlException("Upsert statement not supported");
    }

    @Override
    public void visit(UseStatement use) {
        throw new UnsupportedSqlException("Use statement not supported");
    }

    @Override
    public void visit(Block block) {
        throw new UnsupportedSqlException("Block statement not supported");
    }

    @Override
    public void visit(ValuesStatement values) {
        throw new UnsupportedSqlException("Values statement not supported");
    }

    @Override
    public void visit(DescribeStatement describe) {
        throw new UnsupportedSqlException("Describe statement not supported");
    }

    @Override
    public void visit(ExplainStatement aThis) {
        throw new UnsupportedSqlException("Explain statement not supported");
    }

    @Override
    public void visit(ShowStatement aThis) {
        throw new UnsupportedSqlException("Show statement not supported");
    }

    @Override
    public void visit(Comment comment) {
        //ignore
    }

    @Override
    public void visit(Commit commit) {
        throw new UnsupportedSqlException("Show statement not supported");
    }

    @Override
    public void visit(Delete delete) {
        throw new UnsupportedSqlException("Delete statement not supported");
    }

    @Override
    public void visit(Update update) {
        throw new UnsupportedSqlException("Update statement not supported");
    }

    @Override
    public void visit(Insert insert) {
        throw new UnsupportedSqlException("Insert statement not supported");
    }

    @Override
    public void visit(Replace replace) {
        throw new UnsupportedSqlException("Replace statement not supported");
    }

    @Override
    public void visit(Drop drop) {
        throw new UnsupportedSqlException("Drop statement not supported");
    }

    @Override
    public void visit(Truncate truncate) {
        throw new UnsupportedSqlException("Truncate statement not supported");
    }

    @Override
    public void visit(CreateIndex createIndex) {
        throw new UnsupportedSqlException("Create Index statement not supported");
    }

    @Override
    public void visit(CreateView createView) {
        throw new UnsupportedSqlException("Create View statement not supported");
    }

    @Override
    public void visit(AlterView alterView) {
        throw new UnsupportedSqlException("Alter View statement not supported");
    }

    @Override
    public void visit(Alter alter) {
        throw new UnsupportedSqlException("Alter statement not supported");
    }

    @Override
    public void visit(Statements stmts) {
        for(Statement statement : stmts.getStatements()) {
            statement.accept(this);
        }
    }

    @Override
    public void visit(Execute execute) {
        throw new UnsupportedSqlException("Execute statement not supported");
    }

    @Override
    public void visit(SetStatement set) {
        throw new UnsupportedSqlException("Set statement not supported");
    }

    @Override
    public void visit(ShowColumnsStatement set) {
        throw new UnsupportedSqlException("Show Columns statement not supported");
    }

    @Override
    public void visit(Merge merge) {
        throw new UnsupportedSqlException("Merge statement not supported");
    }

}
