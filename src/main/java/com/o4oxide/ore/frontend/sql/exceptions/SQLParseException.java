package com.o4oxide.ore.frontend.sql.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class SQLParseException extends OreRuntimeException {

    public SQLParseException(String message) {
        super(message);
    }

    public SQLParseException(String message, Throwable ex) {
        super(message, ex);
    }
    public SQLParseException(Throwable ex) {
        super(ex);
    }
}
