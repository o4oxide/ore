package com.o4oxide.ore.frontend.sql.parsers;

import com.o4oxide.ore.frontend.sql.exceptions.UnsupportedSqlException;
import com.o4oxide.ore.frontend.sql.ColumnSpecs;
import com.o4oxide.ore.utils.Checkers;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ColumnSpecsParser {

    private static final List<String> KEYWORDS = Arrays.asList("NOT", "NULL", "DEFAULT");

    public ColumnSpecs parse(List<String> columnSpecsArguments) {
        if (Checkers.isEmpty(columnSpecsArguments)) {
            return null;
        }
        Iterator<String> colSpecsIt = columnSpecsArguments.iterator();
        ColumnSpecs columnSpecs = new ColumnSpecs();
        while(colSpecsIt.hasNext()) {
            String spec = colSpecsIt.next();
            switch(spec) {
                case "NOT": setNotNull(columnSpecs, colSpecsIt); break;
                case "NULL": columnSpecs.setNullable(true); break;
                case "DEFAULT": setDefaultValue(columnSpecs, colSpecsIt); break;
                default: throw new UnsupportedSqlException("Unsupported column specification " + spec);
            }
        }
        return columnSpecs;
    }

    private void setNotNull(ColumnSpecs columnSpecs, Iterator<String> colSpecsIt) {
        if (!colSpecsIt.hasNext() || !colSpecsIt.next().equals("NULL")) {
           throw new UnsupportedSqlException("Was expecting NULL after NOT");
        }
        columnSpecs.setNullable(false);
    }

    private void setDefaultValue(ColumnSpecs columnSpecs, Iterator<String> colSpecsIt) {
        String lookAhead;
        if (!colSpecsIt.hasNext() || KEYWORDS.contains(lookAhead = colSpecsIt.next())) {
            throw new UnsupportedSqlException("Was expecting a value after DEFAULT");
        }
        columnSpecs.setDefaultValue(lookAhead);
    }
}
