package com.o4oxide.ore.frontend.sql.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class UnsupportedSqlException extends OreRuntimeException {
    public UnsupportedSqlException(String message) {
        super(message);
    }
}
