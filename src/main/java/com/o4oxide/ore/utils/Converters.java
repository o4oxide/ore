package com.o4oxide.ore.utils;

import java.lang.reflect.Array;

public final class Converters {

    private Converters() {}

    public static <T extends Enum<T>> Enum<T> getEnum(Class<T> enumClass, String name) {
        if (Checkers.isEmpty(name) || !enumClass.isEnum()) {
            return null;
        }
        for (T enumConstant : enumClass.getEnumConstants()) {
            if (name.equals(enumConstant.name())) {
                return enumConstant;
            }
        }
        return null;
    }

    public static <T extends Enum<T>> Enum<T>[] getEnums(Class<T> enumClass, String[] names) {
        if (Checkers.isEmpty(names) || !enumClass.isEnum()) {
            return null;
        }
        Enum<T>[] enumValues = (Enum<T>[]) Array.newInstance(enumClass, names.length);
        for (int i = 0; i < names.length; i++) {
            enumValues[i] = getEnum(enumClass, names[i]);
        }
        return enumValues;
    }
}
