package com.o4oxide.ore.utils.snowflake;

import java.util.function.Function;
import java.util.function.Supplier;

public final class Creator<PARAM1, INSTANCE> {

    private INSTANCE instance;
    private Supplier<INSTANCE> supplier;
    private Function<PARAM1, INSTANCE> function;

    public Creator(Supplier<INSTANCE> supplier) {
        this.supplier = supplier;
    }

    public Creator(Function<PARAM1, INSTANCE> function) {
        this.function = function;
    }
    
    public INSTANCE singleton() {
        if (instance == null) {
            synchronized (this) {
                if (instance == null) {
                    instance = supplier.get();
                }
            }
        }
        return instance;
    }

    public INSTANCE singleton(PARAM1 param) {
        if (instance == null) {
            synchronized (this) {
                if (instance == null) {
                    instance = function.apply(param);
                }
            }
        }
        return instance;
    }
}
