package com.o4oxide.ore.utils;

import java.util.Collection;
import java.util.Map;

public final class Checkers {

    private Checkers(){}

    //String
    public static boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isBlank(String string) {
        return string == null || string.isBlank();
    }

    public static boolean isNotEmpty(String string) {
        return !isEmpty(string);
    }

    public static boolean isNotBlank(String string) {
        return !isBlank(string);
    }

    public static boolean isNumeric(String string) {
        CharSequence charSequence;
        byte[] strBytes = string.getBytes();
        for (byte strByte : strBytes) {
            if (!Character.isDigit(strByte & 0xff)) {
                return false;
            }
        }
        return true;
    }

    //Collections
    public static <E> boolean isEmpty(Collection<E> collection) {
        return collection == null || collection.isEmpty();
    }

    //Object Array
    public static <E> boolean isEmpty(E[] items) {
        return items == null || items.length == 0;
    }
    public static <E> boolean isNotEmpty(E[] items) {
        return !isEmpty(items);
    }
    public static <E> boolean isNotEmpty(Collection<E> collection) {
        return !isEmpty(collection);
    }

    //Maps
    public static <K,E> boolean isEmpty(Map<K,E> map) {
        return map == null || map.isEmpty();
    }
    public static <K,E> boolean isNotEmpty(Map<K,E> map) {
        return !isEmpty(map);
    }

    //Object
    public static boolean isNull(Object object) {
        return object == null;
    }
    public static boolean isNotNull(Object object) {
        return !isNull(object);
    }

    //Class
    public static boolean isTypeOf(Object instance, Class<?> clazz) {
        if (instance == null || clazz == null) {
            return false;
        }
        return clazz.isAssignableFrom(instance.getClass());
    }
    public static boolean isNotTypeOf(Object instance, Class<?> clazz) {
        return !isTypeOf(instance, clazz);
    }

    //byte[]
    public static <E> boolean isEmpty(byte[] items) {
        return items == null || items.length == 0;
    }
    public static <E> boolean isNotEmpty(byte[] items) {
        return !isEmpty(items);
    }

}
