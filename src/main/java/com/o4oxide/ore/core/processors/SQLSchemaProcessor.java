package com.o4oxide.ore.core.processors;


import com.o4oxide.ore.backend.dbms.OreDBMS;
import com.o4oxide.ore.core.models.schema.RecordSchema;
import com.o4oxide.ore.frontend.sql.parsers.SchemaParser;
import com.o4oxide.ore.utils.snowflake.Creator;


public final class SQLSchemaProcessor implements CommandProcessor<String, Integer> {

    private static final Creator<OreDBMS, SQLSchemaProcessor> creator = new Creator<>(SQLSchemaProcessor::new);

    public static SQLSchemaProcessor create(OreDBMS database) {
        return creator.singleton(database);
    }

    private final OreDBMS database;
    private SQLSchemaProcessor(OreDBMS database) {
        this.database = database;
    }

    @Override
    public Integer process(String ddl) {
        RecordSchema recordSchema = new SchemaParser().parse(ddl);
        return database.createSchema(recordSchema);
    }
}
