package com.o4oxide.ore.core.processors;

public interface CommandProcessor<COMMAND, RESULT> {
    RESULT process(COMMAND command);
}
