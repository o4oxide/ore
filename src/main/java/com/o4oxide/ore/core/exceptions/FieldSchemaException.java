package com.o4oxide.ore.core.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class FieldSchemaException extends OreRuntimeException {
    public FieldSchemaException(String message) {
        super(message);
    }
}
