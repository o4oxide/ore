package com.o4oxide.ore.core.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class FieldExceedsCapacityException extends OreRuntimeException {
    public FieldExceedsCapacityException(String message) {
        super(message);
    }
}
