package com.o4oxide.ore.core.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class FieldValueCastException extends OreRuntimeException {
    public FieldValueCastException(String message) {
        super(message);
    }
}
