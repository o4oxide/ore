package com.o4oxide.ore.core.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class FieldTypeMismatchException extends OreRuntimeException {
    public FieldTypeMismatchException(String message) {
        super(message);
    }
}
