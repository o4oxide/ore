package com.o4oxide.ore.core.models.schema;


import com.o4oxide.ore.core.exceptions.FieldSchemaException;

public class BoundableFieldSchema extends FieldSchema {

    protected int minimum;
    protected int maximum;

    public BoundableFieldSchema(String name, Type type, int[] bounds) {
        super(name, type);
        if (!type.isBoundable()) {
            throw new FieldSchemaException("Type must be boundable");
        }
        computeBounds(bounds);
    }

    public int getMinimum() {
        return minimum;
    }

    public int getMaximum() {
        return maximum;
    }

    private void computeBounds(int[] bounds) {
        if (bounds != null && bounds.length > 0) {
            if (bounds.length > 2) {
                throw new IllegalArgumentException("Only minimum and maximum bounds are supported");
            }
            if (bounds.length == 1) {
                maximum = bounds[0];
            } else {
                minimum = bounds[0];
                maximum = bounds[1];
            }
        }
    }
}