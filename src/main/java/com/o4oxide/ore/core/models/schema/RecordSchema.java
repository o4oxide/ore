package com.o4oxide.ore.core.models.schema;


import com.o4oxide.ore.core.exceptions.FieldSchemaException;

import java.util.LinkedHashSet;

import static com.o4oxide.ore.core.models.schema.FieldSchema.Type.RECORD;

public class RecordSchema extends FieldSchema {

    private static final int MAX_NESTING_DEPTH = 4;
    private static final int MAX_FIELDS_PER_RECORD = 16;

    protected final LinkedHashSet<FieldSchema> fields = new LinkedHashSet<>();
    private int nestingHeight = 0;

    public RecordSchema(String name) {
        super(name, RECORD);
    }

    public RecordSchema addField(FieldSchema field) {
        validate(field);
        if (RECORD == field.type) {
            computeNestingHeight((RecordSchema) field);
        }
        fields.add(field);
        return this;
    }

    public LinkedHashSet<FieldSchema> getFields() {
        return fields;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        throw new FieldSchemaException("Record does not support default value");
    }

    @Override
    protected Object convertToFieldJavaType(String value) {
        return null;
    }

    private void computeNestingHeight(RecordSchema recordField) {
        if (nestingHeight < (recordField.nestingHeight + 1)) {
            nestingHeight = recordField.nestingHeight + 1;
        }
        if (nestingHeight > MAX_NESTING_DEPTH) {
            throw new FieldSchemaException("Exceeded permitted nesting depth (" + MAX_NESTING_DEPTH + ")");
        }
    }

    private void validate(FieldSchema field) {
        if (fields.stream().anyMatch(rec -> rec.getName().equals(field.name))) {
            throw new FieldSchemaException("Field name is already used (" + field.name + ")");
        }
        if (fields.size() + 1 > MAX_FIELDS_PER_RECORD) {
            throw new FieldSchemaException("Exceeded permitted number of fields (" + MAX_FIELDS_PER_RECORD + ")");
        }
    }
}
