package com.o4oxide.ore.core.models.dto;

import java.util.Optional;


public interface Record {

    void setBoolean(String name, boolean value);

    void setBooleanArray(String name, boolean[] values);

    Optional<Boolean> getBoolean(String name);

    Optional<boolean[]> getBooleanArray(String name);

    void setBytes(String name, byte[] value);

    void setBytesArray(String name, byte[][] values);

    Optional<byte[]> getBytes(String name);

    Optional<byte[][]> getBytesArray(String name);

    void setDouble(String name, double value);

    void setDoubleArray(String name, double[] values);

    Optional<Double> getDouble(String name);

    Optional<double[]> getDoubleArray(String name);

    void setEnum(String name, Enum<?> value);

    void setEnumArray(String name, Enum<?>[] values);

    <T extends Enum<T>> Optional<Enum<T>> getEnum(String name, Class<T> enumClass);

    <T extends Enum<T>> Optional<Enum<T>[]> getEnumArray(String name, Class<T> enumClass);

    void setFloat(String name, float value);

    void setFloatArray(String name, float[] values);

    Optional<Float> getFloat(String name);

    Optional<float[]> getFloatArray(String name);

    void setInteger(String name, int value);

    void setIntegerArray(String name, int[] value);

    Optional<Integer> getInteger(String name);

    Optional<int[]> getIntegerArray(String name);

    void setLong(String name, long value);

    void setLongArray(String name, long[] value);

    Optional<Long> getLong(String name);

    Optional<long[]> getLongArray(String name);

    void setRecord(String name, Record record);

    void setRecordArray(String name, Record[] records);

    Optional<Record> getRecord(String name);

    Optional<Record[]> getRecordArray(String name);

    void setString(String name, String value);

    void setStringArray(String name, String[] value);

    Optional<String> getString(String name);

    Optional<String[]> getStringArray(String name);
}
