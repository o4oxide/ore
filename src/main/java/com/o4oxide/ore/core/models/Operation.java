package com.o4oxide.ore.core.models;

public class Operation {

    public enum Operator {
        PLUS,
        MINUS,
        MULTIPLY,
        DIVISION,
        MODULO,
        AND,
        OR,
        XOR,
        EQUALS,
        NOT_EQUALS,
        GREATER,
        LESS,
        GREATER_EQUALS,
        LESS_EQUALS,
        CONCAT,
        NOT,
    }

    private final Operand leftOperand;
    private final Operator operator;
    private final Operand rightOperand;

    public Operation(Operand leftOperand, Operator operator, Operand rightOperand) {
        this.leftOperand = leftOperand;
        this.operator = operator;
        this.rightOperand = rightOperand;
    }
}
