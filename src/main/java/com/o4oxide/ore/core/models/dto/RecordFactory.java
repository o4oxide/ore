package com.o4oxide.ore.core.models.dto;

import static com.o4oxide.ore.core.models.schema.FieldSchema.Type;

public abstract class RecordFactory<T> {

    public abstract Field<Boolean> createBooleanField(Type type, String name, boolean value, T...param);
    public abstract Field<byte[]>  createBytesField  (Type type, String name, byte[] value,  T...param);
    public abstract Field<Double>  createDoubleField (Type type, String name, double value,  T...param);
    public abstract Field<Enum<?>> createEnumField   (Type type, String name, Enum<?> value, T...param);
    public abstract Field<Float>   createFloatField  (Type type, String name, float value,   T...param);
    public abstract Field<Integer> createIntegerField(Type type, String name, int value,     T...param);
    public abstract Field<Long>    createLongField   (Type type, String name, long value,    T...param);
    public abstract Record createRecord      (String name, T...param);
}
