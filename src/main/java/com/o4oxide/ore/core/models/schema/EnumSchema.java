package com.o4oxide.ore.core.models.schema;


import com.o4oxide.ore.core.exceptions.FieldSchemaException;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.o4oxide.ore.core.models.schema.FieldSchema.Type.ENUM;

public class EnumSchema extends FieldSchema {

    private final Set<String> values;

    public EnumSchema(String name, List<String> values) {
        super(name, ENUM);
        if (values == null || values.isEmpty()) {
            throw new IllegalArgumentException("values cannot be empty");
        }
        this.values = Collections.unmodifiableSet(new LinkedHashSet<>(values));
    }

    public Set<String> getValues() {
        return values;
    }

    @Override
    protected Object convertToFieldJavaType(String value) {
        return values.stream()
                .filter(val -> val.equals(value))
                .findFirst()
                .orElseThrow(()-> new FieldSchemaException("Enum does not know the value " + value));
    }
}