package com.o4oxide.ore.core.models.schema;

import com.o4oxide.ore.core.exceptions.FieldSchemaException;

import java.util.Objects;

public class FieldSchema {

    public enum Type {
        BOOLEAN(false),
        INTEGER(false),
        LONG(false),
        FLOAT(false),
        DOUBLE(false),
        BYTES(true),
        STRING(true),
        RECORD(false),
        ENUM(false);

        boolean boundable;

        Type(boolean boundable) {
            this.boundable = boundable;
        }

        public boolean isBoundable() {
            return boundable;
        }
    }

    private static final int MAX_LENGTH_OF_FIELD_NAME = 64;

    protected final String name;
    protected final Type type;

    protected boolean nullable = true;
    protected Object defaultValue;
    protected int arraySize;

    public FieldSchema(String name, Type type) {
        if (name.length() > MAX_LENGTH_OF_FIELD_NAME) {
            throw new FieldSchemaException("Exceeded permitted length for field names (" + MAX_LENGTH_OF_FIELD_NAME + ")");
        }
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public boolean isNullable() {
        return nullable;
    }

    public int getArraySize() {
        return arraySize;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = convertToFieldJavaType(defaultValue);
    }

    public void setArraySize(int arraySize) {
        this.arraySize = arraySize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FieldSchema)) return false;
        FieldSchema that = (FieldSchema) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


    protected Object convertToFieldJavaType(String value) {
        switch(type) {
            case BOOLEAN: return Boolean.parseBoolean(value);
            case INTEGER: return Integer.parseInt(value);
            case LONG:    return Long.parseLong(value);
            case FLOAT:   return Float.parseFloat(value);
            case DOUBLE:  return Double.parseDouble(value);
            case BYTES:   return value.getBytes();
            case STRING:  return value;
            case RECORD:  throw new FieldSchemaException("Should call RecordSchema.convertToFieldJavaType instead");
            case ENUM:    throw new FieldSchemaException("Should call EnumSchema.convertToFieldJavaType instead");
            default: throw new FieldSchemaException("Unknown field type " + type.name());
        }
    }
}