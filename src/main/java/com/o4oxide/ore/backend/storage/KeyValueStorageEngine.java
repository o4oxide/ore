package com.o4oxide.ore.backend.storage;

public interface KeyValueStorageEngine<K, V> extends StorageEngine {
    void save(K key, V value);
    V read(K key);
}
