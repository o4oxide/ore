package com.o4oxide.ore.backend.storage.rocksdb;

import com.o4oxide.ore.backend.storage.KeyValueStorageEngine;
import com.o4oxide.ore.core.models.schema.RecordSchema;
import org.rocksdb.*;

public class RocksDBStorageEngine implements KeyValueStorageEngine<byte[], byte[]> {

    private RocksDB rocksDB;

    @Override
    public void save(byte[] key, byte[] value) {
        //rocksDB.put(key, value);
    }

    @Override
    public byte[] read(byte[] key) {
        try {
           return rocksDB.get(key);
        } catch (RocksDBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ColumnFamilyHandle createColumnFamily(RecordSchema recordSchema) {
        return null;
    }
}

