package com.o4oxide.ore.backend.dbms;

import com.o4oxide.ore.core.models.dto.Record;
import com.o4oxide.ore.core.models.schema.RecordSchema;

public interface OreDBMS {

    int createSchema(RecordSchema recordSchema);
    void saveRecord(Record record);
}
