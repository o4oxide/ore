package com.o4oxide.ore.backend.dbms.rocksprotobuf.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class ConflictException extends OreRuntimeException {
    public ConflictException(String message) {
        super(message);
    }
}
