package com.o4oxide.ore.backend.dbms.rocksprotobuf;

import com.o4oxide.ore.core.models.schema.FieldSchema;
import com.o4oxide.ore.core.models.schema.RecordSchema;

import static com.o4oxide.ore.backend.dbms.rocksprotobuf.SystemSchemaDefinitions.RECORD_META_SCHEMA_KEYS.*;

final class SystemSchemaDefinitions {

    private SystemSchemaDefinitions() {}

    private static final String NAMESPACE = "com.o4oxide.ore";

    private static final String RECORD_META_SCHEMA_NAME = "record_meta_schema";
    static class RECORD_META_SCHEMA_KEYS {
        static final String schema_name = "schema_name";
        static final String version = "version";
        static final String date = "date";
        static final String current_schema = "current_schema";
        static final String options = "options";
        static final String options_key = "key";
        static final String options_value = "value";
    }

    static final RecordSchema RECORD_META_SCHEMA = buildRecordMetaSchema();

    private static RecordSchema buildRecordMetaSchema() {

        RecordSchema recordSchema = new RecordSchema(schema_name);
        recordSchema.addField(new FieldSchema(version, FieldSchema.Type.INTEGER));
        recordSchema.addField(new FieldSchema(date, FieldSchema.Type.LONG));
        recordSchema.addField(new FieldSchema(current_schema, FieldSchema.Type.BYTES));
        RecordSchema optionsRecord = new RecordSchema(options);
        optionsRecord.addField(new FieldSchema(options_key, FieldSchema.Type.STRING));
        optionsRecord.addField(new FieldSchema(options_value, FieldSchema.Type.STRING));
        recordSchema.addField(optionsRecord);

        return recordSchema;
    }

}
