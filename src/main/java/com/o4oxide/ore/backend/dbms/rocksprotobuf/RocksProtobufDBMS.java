package com.o4oxide.ore.backend.dbms.rocksprotobuf;

import com.o4oxide.ore.backend.dbms.OreDBMS;
import com.o4oxide.ore.backend.dbms.rocksprotobuf.exceptions.ConflictException;
import com.o4oxide.ore.backend.serialization.protobuf.ProtobufSerializer;
import com.o4oxide.ore.backend.storage.rocksdb.RocksDBStorageEngine;
import com.o4oxide.ore.core.models.dto.Record;
import com.o4oxide.ore.core.models.schema.RecordSchema;
import org.rocksdb.ColumnFamilyHandle;

import java.util.HashMap;
import java.util.Map;


public class RocksProtobufDBMS implements OreDBMS {

    private final RocksDBStorageEngine storageEngine;
    private final ProtobufSerializer serializer;
    private final Map<String, ColumnFamilyHandle> columnFamilyHandles = new HashMap<>();

    public RocksProtobufDBMS() {
        this.storageEngine = new RocksDBStorageEngine();
        this.serializer = new ProtobufSerializer();
    }

    @Override
    public int createSchema(RecordSchema recordSchema) {
        ColumnFamilyHandle columnFamilyHandle = columnFamilyHandles.get(recordSchema.getName());
        if (columnFamilyHandle != null) {
            throw new ConflictException("schema " + recordSchema.getName() + "already exists");
        }

        columnFamilyHandle = storageEngine.createColumnFamily(recordSchema);
        columnFamilyHandles.put(recordSchema.getName(), columnFamilyHandle);

        byte[] schemaBytes = serializer.buildSchema(recordSchema);
        storageEngine.save(recordSchema.getName().getBytes(), schemaBytes);
        return 1;
    }

    @Override
    public void saveRecord(Record record) {

    }

}
