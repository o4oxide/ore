package com.o4oxide.ore.backend.serialization.protobuf.fields;

import com.o4oxide.ore.core.models.schema.EnumSchema;
import com.o4oxide.ore.utils.Checkers;

public class ProtobufEnumSchema extends ProtobufFieldSchema {

    private final String[] values;

    public ProtobufEnumSchema(int fieldNumber, EnumSchema enumSchema) {
        super(fieldNumber, enumSchema);
        int index = 0;
        values = new String[enumSchema.getValues().size()];
        String defaultValue = (String) enumSchema.getDefaultValue();
        if (Checkers.isNotBlank(defaultValue)) {
            values[index++] = defaultValue;
        }
        for (String val : enumSchema.getValues()) {
            values[index++] = val;
        }
    }

   public int getEnumOrdinal(String enumName) {
        for(int i = 0; i < values.length; i++) {
            if (values[i].equals(enumName)) {
                return i;
            }
        }
        return -1;
   }
}
