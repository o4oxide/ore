package com.o4oxide.ore.backend.serialization.protobuf.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class UnsupportedBufferSizeException extends OreRuntimeException {

    public UnsupportedBufferSizeException(String message) {
        super(message);
    }
}
