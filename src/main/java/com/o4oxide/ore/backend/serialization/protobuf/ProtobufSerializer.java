package com.o4oxide.ore.backend.serialization.protobuf;


import com.o4oxide.ore.backend.serialization.BufferCache;
import com.o4oxide.ore.backend.serialization.SchemableSerializer;
import com.o4oxide.ore.backend.serialization.protobuf.fields.ProtobufRecord;
import com.o4oxide.ore.core.models.dto.Record;
import com.o4oxide.ore.core.models.schema.RecordSchema;

import static com.o4oxide.ore.backend.serialization.protobuf.fields.ProtobufRecordSchema.RECORD_META_SCHEMA;


public class ProtobufSerializer implements SchemableSerializer<byte[], Record> {

    private static final ThreadLocal<BufferCache> bufferCache = ThreadLocal.withInitial(() -> new BufferCache(5));

    @Override
    public byte[] buildSchema(RecordSchema recordSchema) {
        ProtobufRecord metaSchemaRecord = new ProtobufRecord(RECORD_META_SCHEMA);
        RECORD_META_SCHEMA.get
        metaSchemaRecord.set

        return metaSchemaRecord;
    }

    @Override
    public byte[] serialize(Record record) {
        return new byte[0];
    }

    @Override
    public Record deserialize(byte[] bytes) {
        return null;
    }

}
