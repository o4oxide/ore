package com.o4oxide.ore.backend.serialization.protobuf.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class ProtobufFieldIOException extends OreRuntimeException {
    public ProtobufFieldIOException(String message) {
        super(message);
    }

    public ProtobufFieldIOException(String message, Throwable ex) {
        super(message, ex);
    }

    public ProtobufFieldIOException(Throwable ex) {
        super(ex);
    }
}
