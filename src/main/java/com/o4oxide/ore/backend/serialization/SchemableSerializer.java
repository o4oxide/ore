package com.o4oxide.ore.backend.serialization;

import com.o4oxide.ore.core.models.schema.RecordSchema;

public interface SchemableSerializer<SCHEMA, MESSAGE> extends Serializer<MESSAGE>{
    SCHEMA buildSchema(RecordSchema recordSchema);
}
