package com.o4oxide.ore.backend.serialization.protobuf.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class ProtobufRecordLockedException extends OreRuntimeException {
    public ProtobufRecordLockedException(String message) {
        super(message);
    }
}
