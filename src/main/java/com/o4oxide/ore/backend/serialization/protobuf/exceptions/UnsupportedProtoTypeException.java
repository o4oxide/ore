package com.o4oxide.ore.backend.serialization.protobuf.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class UnsupportedProtoTypeException extends OreRuntimeException {

    public UnsupportedProtoTypeException(String message) {
        super(message);
    }
}
