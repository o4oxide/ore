package com.o4oxide.ore.backend.serialization.protobuf.fields;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.WireFormat;
import com.o4oxide.ore.backend.serialization.protobuf.exceptions.*;
import com.o4oxide.ore.core.exceptions.FieldExceedsCapacityException;
import com.o4oxide.ore.core.models.dto.Record;
import com.o4oxide.ore.utils.Checkers;
import com.o4oxide.ore.utils.Converters;

import java.io.IOException;
import java.util.Optional;

import static com.o4oxide.ore.core.models.schema.FieldSchema.Type;
import static com.o4oxide.ore.core.models.schema.FieldSchema.Type.*;


public class ProtobufRecord implements Record {

    private final ProtobufRecordSchema schema;
    private final Object[] fields;
    private boolean lockedForWrite;
    private int recordSize = -1;


    public ProtobufRecord(ProtobufRecordSchema schema) {
        this.schema = schema;
        this.fields = new Object[schema.getFieldsCount()];
    }

    @Override
    public void setBoolean(String name, boolean value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, BOOLEAN);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setBooleanArray(String name, boolean[] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, BOOLEAN);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public Optional<Boolean> getBoolean(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, BOOLEAN);
        return Optional.ofNullable((Boolean) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<boolean[]> getBooleanArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, BOOLEAN);
        return Optional.ofNullable((boolean[]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public void setBytes(String name, byte[] value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, BYTES);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setBytesArray(String name, byte[][] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, BYTES);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public Optional<byte[]> getBytes(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, BYTES);
        return Optional.ofNullable((byte[]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<byte[][]> getBytesArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, BYTES);
        return Optional.ofNullable((byte[][]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public void setDouble(String name, double value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, DOUBLE);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setDoubleArray(String name, double[] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, DOUBLE);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public Optional<Double> getDouble(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, DOUBLE);
        return Optional.ofNullable((Double) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<double[]> getDoubleArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, DOUBLE);
        return Optional.ofNullable((double[]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public void setEnum(String name, Enum<?> value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufEnumSchema fieldSchema = (ProtobufEnumSchema) getFieldSchema(name, ENUM);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setEnumArray(String name, Enum<?>[] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufEnumSchema fieldSchema = (ProtobufEnumSchema) getArrayFieldSchema(name, ENUM);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public <T extends Enum<T>> Optional<Enum<T>> getEnum(String name, Class<T> enumClass) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, ENUM);
        return Optional.ofNullable(Converters.getEnum(enumClass, (String) fields[fieldSchema.getFieldNumber()]));
    }

    @Override
    public <T extends Enum<T>> Optional<Enum<T>[]> getEnumArray(String name, Class<T> enumClass) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, ENUM);
        return Optional.ofNullable(Converters.getEnums(enumClass, (String[]) fields[fieldSchema.getFieldNumber()]));
    }

    @Override
    public void setFloat(String name, float value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, FLOAT);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setFloatArray(String name, float[] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, FLOAT);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public Optional<Float> getFloat(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, FLOAT);
        return Optional.ofNullable((Float) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<float[]> getFloatArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, FLOAT);
        return Optional.ofNullable((float[]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public void setInteger(String name, int value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, INTEGER);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setIntegerArray(String name, int[] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, INTEGER);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public Optional<Integer> getInteger(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, INTEGER);
        return Optional.ofNullable((Integer) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<int[]> getIntegerArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, INTEGER);
        return Optional.ofNullable((int[]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public void setLong(String name, long value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, LONG);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setLongArray(String name, long[] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, LONG);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public Optional<Long> getLong(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, LONG);
        return Optional.ofNullable((Long) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<long[]> getLongArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, DOUBLE);
        return Optional.ofNullable((long[]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public void setRecord(String name, Record record) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufRecordSchema fieldSchema = (ProtobufRecordSchema) getFieldSchema(name, RECORD);
        if (Checkers.isNotTypeOf(record, ProtobufRecord.class)) {
            throw new IllegalArgumentException("Expecting a ProtobufRecord");
        }
        ProtobufRecord protobufRecord = (ProtobufRecord) record;
        fields[fieldSchema.getFieldNumber()] = protobufRecord;
    }

    @Override
    public void setRecordArray(String name, Record[] records) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, RECORD);
        if (records.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        if (Checkers.isNotTypeOf(records, ProtobufRecord[].class)) {
            throw new IllegalArgumentException("Expecting a ProtobufRecord");
        }
        ProtobufRecord[] protobufRecords = (ProtobufRecord[]) records;
        fields[fieldSchema.getFieldNumber()] = protobufRecords;
    }

    @Override
    public Optional<Record> getRecord(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, RECORD);
        return Optional.ofNullable((Record) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<Record[]> getRecordArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, RECORD);
        return Optional.ofNullable((Record[]) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public void setString(String name, String value) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, STRING);
        fields[fieldSchema.getFieldNumber()] = value;
    }

    @Override
    public void setStringArray(String name, String[] values) {
        if (lockedForWrite) {
            throw new ProtobufRecordLockedException("Record has been locked for write, no updates allowed");
        }
        ProtobufFieldSchema fieldSchema = getArrayFieldSchema(name, STRING);
        if (values.length < fieldSchema.getArraySize()) {
            throw new FieldExceedsCapacityException("Array size exceeds maximum: " + fieldSchema.getArraySize());
        }
        fields[fieldSchema.getFieldNumber()] = values;
    }

    @Override
    public Optional<String> getString(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, STRING);
        return Optional.ofNullable((String) fields[fieldSchema.getFieldNumber()]);
    }

    @Override
    public Optional<String[]> getStringArray(String name) {
        ProtobufFieldSchema fieldSchema = getFieldSchema(name, STRING);
        return Optional.ofNullable((String[]) fields[fieldSchema.getFieldNumber()]);
    }

    public void write(CodedOutputStream stream) {
        prepareForWrite();
        for (ProtobufFieldSchema fieldSchema : schema.getFields()) {
            Object val = fields[fieldSchema.getFieldNumber()];
            if (val != null) {
                writeInternal(stream, schema, val);
            }
        }
    }

    public int prepareForWrite() {
        lockedForWrite = true;
        return computeRecordSize();
    }

    private ProtobufFieldSchema getProtobufFieldSchema(String name, Type type) {
        ProtobufFieldSchema fieldSchema = schema.getProtobufFieldSchema(name);
        if (fieldSchema == null) {
            throw new UnknownFieldException("field " + name + " does not exist");
        }
        if (type != fieldSchema.getType()) {
            throw new SchemaValidationException("Invalid schema Type: " + schema.getType() + " expected: " + type);
        }
        return fieldSchema;
    }

    private ProtobufFieldSchema getFieldSchema(String name, Type type) {
        ProtobufFieldSchema schema = getProtobufFieldSchema(name, type);
        if (schema.isArray()) {
            throw new IllegalArgumentException("please use the write array variant instead");
        }
        return schema;
    }

    private ProtobufFieldSchema getArrayFieldSchema(String name, Type type) {
        ProtobufFieldSchema schema = getProtobufFieldSchema(name, type);
        if (! schema.isArray()) {
            throw new IllegalArgumentException("please use the single value write variant instead");
        }
        return schema;
    }

    private int computeRecordSize() {
        if (recordSize > 0 && lockedForWrite) {
            return recordSize;
        }
        for (ProtobufFieldSchema fieldSchema : schema.getFields()) {
            Object val = fields[fieldSchema.getFieldNumber()];
            if (val != null) {
                recordSize += computeFieldSize(fieldSchema, val);
            }
        }
        return recordSize;
    }

    private int computeFieldSize(ProtobufFieldSchema fieldSchema, Object value) {
        switch (fieldSchema.getType()) {
            case BOOLEAN:
                return computeBooleanSize(fieldSchema, value);
            case BYTES:
                return computeBytesSize(fieldSchema, value);
            case DOUBLE:
                return computeDoubleSize(fieldSchema, value);
            case ENUM:
                return computeEnumSize(fieldSchema, value);
            case FLOAT:
                return computeFloatSize(fieldSchema, value);
            case INTEGER:
                return computeIntegerSize(fieldSchema, value);
            case LONG:
                return computeLongSize(fieldSchema, value);
            case RECORD:
                return computeRecordValueSize(fieldSchema, value);
            case STRING:
                return computeStringSize(fieldSchema, value);
        }
        return 0;
    }

    private int computeBooleanSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        if (fieldSchema.isArray()) {
            for (boolean val : (boolean[]) value) {
                size += CodedOutputStream.computeBoolSize(fieldNo, val);
            }
        } else {
            size += CodedOutputStream.computeBoolSize(fieldNo, (boolean) value);
        }
        return size;
    }

    private int computeBytesSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        if (fieldSchema.isArray()) {
            for (byte[] val : (byte[][]) value) {
                if (Checkers.isNotEmpty(val)) {
                    size += CodedOutputStream.computeByteArraySize(fieldNo, val);
                }
            }
        } else {
            if (Checkers.isNotEmpty((byte[]) value)) {
                size += CodedOutputStream.computeByteArraySize(fieldNo, (byte[]) value);
            }
        }
        return size;
    }

    private int computeDoubleSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        if (fieldSchema.isArray()) {
            for (double val : (double[]) value) {
                size += CodedOutputStream.computeDoubleSize(fieldNo, val);
            }
        } else {
            size += CodedOutputStream.computeDoubleSize(fieldNo, (double) value);
        }
        return size;
    }

    private int computeEnumSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        ProtobufEnumSchema enumSchema = (ProtobufEnumSchema) fieldSchema;
        if (enumSchema.isArray()) {
            for (Enum<?> val : (Enum<?>[]) value) {
                int ordinal = enumSchema.getEnumOrdinal(val.name());
                if (ordinal < 0) {
                    throw new IllegalArgumentException("Unknown enum value: " + val.name());
                }
                size += CodedOutputStream.computeEnumSize(fieldNo, ordinal);
            }
        } else {
            Enum<?> enumVal = (Enum<?>) value;
            int ordinal = enumSchema.getEnumOrdinal(enumVal.name());
            if (ordinal < 0) {
                throw new IllegalArgumentException("Unknown enum value: " + enumVal.name());
            }
            size += CodedOutputStream.computeEnumSize(fieldNo, ordinal);
        }
        return size;
    }

    private int computeFloatSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        if (fieldSchema.isArray()) {
            for (float val : (float[]) value) {
                size += CodedOutputStream.computeFloatSize(fieldNo, val);
            }
        } else {
            size += CodedOutputStream.computeFloatSize(fieldNo, (float) value);
        }
        return size;
    }

    private int computeIntegerSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        if (fieldSchema.isArray()) {
            for (int val : (int[]) value) {
                size += CodedOutputStream.computeSInt32Size(fieldNo, val);
            }
        } else {
            size += CodedOutputStream.computeSInt32Size(fieldNo, (int) value);
        }
        return size;
    }

    private int computeLongSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        if (fieldSchema.isArray()) {
            for (long val : (long[]) value) {
                size += CodedOutputStream.computeSInt64Size(fieldNo, val);
            }
        } else {
            size += CodedOutputStream.computeSInt64Size(fieldNo, (long) value);
        }
        return size;
    }

    private int computeRecordValueSize(ProtobufFieldSchema fieldSchema, Object value) {
        int size = 0;
        int tagSize = CodedOutputStream.computeTagSize(fieldSchema.getFieldNumber());
        if (fieldSchema.isArray()) {
            for (ProtobufRecord val : (ProtobufRecord[]) value) {
                size += tagSize + val.prepareForWrite();
            }
        } else {
            size += tagSize + ((ProtobufRecord) value).prepareForWrite();
        }
        return size;
    }

    private int computeStringSize(ProtobufFieldSchema fieldSchema, Object value) {
        int fieldNo = fieldSchema.getFieldNumber();
        int size = 0;
        if (fieldSchema.isArray()) {
            for (String val : (String[]) value) {
                if (Checkers.isNotBlank(val)) {
                    size += CodedOutputStream.computeStringSize(fieldNo, val);
                }
            }
        } else {
            String val = (String) value;
            if (Checkers.isNotBlank(val)) {
                size += CodedOutputStream.computeStringSize(fieldNo, val);
            }
        }
        return size;
    }

    private void writeInternal(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) {
        try {
            switch (fieldSchema.getType()) {
                case BOOLEAN:
                    writeBoolean(stream, fieldSchema, value);
                    break;
                case BYTES:
                    writeBytes(stream, fieldSchema, value);
                    break;
                case DOUBLE:
                    writeDouble(stream, fieldSchema, value);
                    break;
                case ENUM:
                    writeEnum(stream, fieldSchema, value);
                    break;
                case FLOAT:
                    writeFloat(stream, fieldSchema, value);
                    break;
                case INTEGER:
                    writeInteger(stream, fieldSchema, value);
                    break;
                case LONG:
                    writeLong(stream, fieldSchema, value);
                    break;
                case RECORD:
                    writeRecord(stream, fieldSchema, value);
                    break;
                case STRING:
                    writeString(stream, fieldSchema, value);
                    break;
                default:
                    throw new UnsupportedProtoTypeException("type " + fieldSchema.getType() + " is not supported");
            }
        } catch (IOException ex) {
            throw new ProtobufFieldIOException(ex);
        }
    }

    private void writeBoolean(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (boolean val : (boolean[]) value) {
                stream.writeBool(fieldNo, val);
            }
        } else {
            stream.writeBool(fieldNo, (boolean) value);
        }
    }

    private void writeBytes(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (byte[] val : (byte[][]) value) {
                if (Checkers.isNotEmpty(val)) {
                    stream.writeByteArray(fieldNo, val);
                }
            }
        } else {
            if (Checkers.isNotEmpty((byte[]) value)) {
                stream.writeByteArray(fieldNo, (byte[]) value);
            }
        }
    }

    private void writeDouble(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (double val : (double[]) value) {
                stream.writeDouble(fieldNo, val);
            }
        } else {
            stream.writeDouble(fieldNo, (double) value);
        }
    }

    private void writeEnum(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        ProtobufEnumSchema enumSchema = (ProtobufEnumSchema) fieldSchema;
        if (enumSchema.isArray()) {
            for (Enum<?> val : (Enum<?>[]) value) {
                int ordinal = enumSchema.getEnumOrdinal(val.name());
                if (ordinal < 0) {
                    throw new IllegalArgumentException("Unknown enum value: " + val.name());
                }
                stream.writeDouble(fieldNo, ordinal);
            }
        } else {
            Enum<?> enumVal = (Enum<?>) value;
            int ordinal = enumSchema.getEnumOrdinal(enumVal.name());
            if (ordinal < 0) {
                throw new IllegalArgumentException("Unknown enum value: " + enumVal.name());
            }
            stream.writeDouble(fieldNo, ordinal);
        }
    }

    private void writeFloat(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (float val : (float[]) value) {
                stream.writeFloat(fieldNo, val);
            }
        } else {
            stream.writeFloat(fieldNo, (float) value);
        }
    }

    private void writeInteger(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (int val : (int[]) value) {
                stream.writeSInt32(fieldNo, val);
            }
        } else {
            stream.writeSInt32(fieldNo, (int) value);
        }
    }

    private void writeLong(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (long val : (long[]) value) {
                stream.writeSInt64(fieldNo, val);
            }
        } else {
            stream.writeSInt64(fieldNo, (long) value);
        }
    }

    private void writeRecord(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (ProtobufRecord val : (ProtobufRecord[]) value) {
                int recordValSize = val.prepareForWrite();
                if (recordValSize > 0) {
                    stream.writeTag(fieldNo, WireFormat.WIRETYPE_LENGTH_DELIMITED);
                    stream.writeUInt32NoTag(val.prepareForWrite());
                    val.write(stream);
                }
            }
        } else {
            ProtobufRecord recordValue = (ProtobufRecord) value;
            int recordValSize = recordValue.prepareForWrite();
            if (recordValSize > 0) {
                stream.writeTag(fieldNo, WireFormat.WIRETYPE_LENGTH_DELIMITED);
                stream.writeUInt32NoTag(recordValSize);
                recordValue.write(stream);
            }
        }
    }

    private void writeString(CodedOutputStream stream, ProtobufFieldSchema fieldSchema, Object value) throws IOException {
        int fieldNo = fieldSchema.getFieldNumber();
        if (fieldSchema.isArray()) {
            for (String val : (String[]) value) {
                if (Checkers.isNotBlank(val)) {
                    stream.writeString(fieldNo, val);
                }
            }
        } else {
            String val = (String) value;
            if (Checkers.isNotBlank(val)) {
                stream.writeString(fieldNo, val);
            }
        }
    }

}
