package com.o4oxide.ore.backend.serialization.protobuf.fields;


import com.google.protobuf.FieldType;
import com.o4oxide.ore.backend.serialization.protobuf.exceptions.UnsupportedProtoTypeException;
import com.o4oxide.ore.core.models.schema.FieldSchema;

public class ProtobufFieldSchema {

    private final int fieldNumber;
    private final FieldSchema fieldSchema;

    public ProtobufFieldSchema(int fieldNumber, FieldSchema fieldSchema) {
        this.fieldNumber = fieldNumber;
        this.fieldSchema = fieldSchema;
    }

    public int getFieldNumber() {
        return fieldNumber;
    }

    public String getName() {
        return fieldSchema.getName();
    }

    public FieldSchema.Type getType() {
        return fieldSchema.getType();
    }

    public boolean isArray() {
        return this.fieldSchema.getArraySize() > 0;
    }

    public int getArraySize() {
        return this.fieldSchema.getArraySize();
    }

    public boolean isNullable() {
        return this.fieldSchema.isNullable();
    }

    public Object getDefaultValue() {
        return this.fieldSchema.getDefaultValue();
    }


    static String getProtoTypeName(FieldSchema.Type type) {
        switch (type) {
            case BOOLEAN: return FieldType.BOOL.name().toLowerCase();
            case BYTES:   return FieldType.BYTES.name().toLowerCase();
            case DOUBLE:  return FieldType.DOUBLE.name().toLowerCase();
            case ENUM:    return FieldType.ENUM.name().toLowerCase();
            case FLOAT:   return FieldType.FLOAT.name().toLowerCase();
            case INTEGER: return FieldType.SINT32.name().toLowerCase();
            case LONG:    return FieldType.SINT64.name().toLowerCase();
            case RECORD:  return FieldType.MESSAGE.name().toLowerCase();
            case STRING:  return FieldType.STRING.name().toLowerCase();
        }
        throw new UnsupportedProtoTypeException("Cannot convert type " + type + " to proto type");
    }
}
