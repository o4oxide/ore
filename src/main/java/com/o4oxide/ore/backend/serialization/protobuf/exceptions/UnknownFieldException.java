package com.o4oxide.ore.backend.serialization.protobuf.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class UnknownFieldException extends OreRuntimeException {
    public UnknownFieldException(String message) {
        super(message);
    }
}
