package com.o4oxide.ore.backend.serialization.protobuf.fields;

import com.o4oxide.ore.core.models.schema.EnumSchema;
import com.o4oxide.ore.core.models.schema.FieldSchema;
import com.o4oxide.ore.core.models.schema.RecordSchema;

import java.util.*;

import static com.o4oxide.ore.core.models.schema.FieldSchema.Type.*;

public class ProtobufRecordSchema extends ProtobufFieldSchema  {

    public static final ProtobufRecordSchema RECORD_META_SCHEMA = buildRecordMetaSchema();

    private final Map<String, ProtobufFieldSchema> protoFieldSchemas = new LinkedHashMap<>();

    public ProtobufRecordSchema(int fieldNumber, RecordSchema recordSchema) {
        super(fieldNumber, recordSchema);
        enumerateFieldSchemas(recordSchema.getFields());
    }

    public ProtobufFieldSchema getProtobufFieldSchema(String name) {
        return protoFieldSchemas.get(name);
    }

    public int getFieldsCount() {
        return protoFieldSchemas.size();
    }

    public Collection<ProtobufFieldSchema> getFields() {
        return protoFieldSchemas.values();
    }

    /*
     * message meta {
     *     string name      = 1;
     *     string type      = 2;
     *     bool nullable    = 3;
     *     T default_value  = 4;
     *     bool repeatable  = 5;
     *     bool boundable   = 6;
     *     int min          = 7;
     *     int max          = 8;
     * }
     *
     * message field_meta_schema {
     *     meta meta_data = 1;
     *     repeated string enum_values = 2; // used when type == ENUM
     * }
     *
     *  message record_meta_schema {
     *     meta meta_data = 1;
     *     repeated field aField = 2;
     *  }
     */
    private static ProtobufRecordSchema buildRecordMetaSchema() {
        RecordSchema meta = createMetaSchema();
        RecordSchema fieldMetaSchema = new RecordSchema("field_meta_schema");
        fieldMetaSchema.addField(meta);
        FieldSchema enumValuesField = new FieldSchema("enum_values", STRING);
        enumValuesField.setArraySize(Integer.MAX_VALUE);
        fieldMetaSchema.addField(enumValuesField);

        RecordSchema recordMetaSchema = new RecordSchema("record_meta_schema");
        recordMetaSchema.addField(meta);
        recordMetaSchema.addField(fieldMetaSchema);

        return new ProtobufRecordSchema(0, recordMetaSchema);
    }

    private static RecordSchema createMetaSchema() {
        RecordSchema meta = new RecordSchema("meta");

        meta.addField(new FieldSchema("name", STRING));
        meta.addField(new FieldSchema("type", STRING));
        meta.addField(new FieldSchema("nullable", BOOLEAN));
        meta.addField(new FieldSchema("defaultValue", null));//Actual type depends on the schema type
        meta.addField(new FieldSchema("repeatable", BOOLEAN));
        meta.addField(new FieldSchema("boundable", BOOLEAN));
        meta.addField(new FieldSchema("min", INTEGER));
        meta.addField(new FieldSchema("max", INTEGER));

        return meta;
    }

    private void enumerateFieldSchemas(LinkedHashSet<FieldSchema> fieldSchemas) {
        int index = 0;
        for (FieldSchema fieldSchema : fieldSchemas) {
            ProtobufFieldSchema protoFieldSchema;
            if (RECORD == fieldSchema.getType()) {
                RecordSchema recordSchema = (RecordSchema) fieldSchema;
                protoFieldSchema = new ProtobufRecordSchema(++index, recordSchema);
            } else if (ENUM == fieldSchema.getType()) {
                protoFieldSchema = new ProtobufEnumSchema(++index, (EnumSchema) fieldSchema);
            } else {
                protoFieldSchema = new ProtobufFieldSchema(++index, fieldSchema);
            }
            protoFieldSchemas.putIfAbsent(fieldSchema.getName(), protoFieldSchema);
        }
    }
}
