package com.o4oxide.ore.backend.serialization;


import com.o4oxide.ore.backend.serialization.protobuf.exceptions.UnsupportedBufferSizeException;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

/**
 * A cache of byte buffers of different incremental sizes.
 * This class is intended to be used in a ThreadLocal
 */
public class BufferCache {

    private enum BuffSize {
        TINY(128),
        SMALL(256),
        MEDIUM(512),
        BIG(1024),
        HUGE(2048),
        MEGA(4096);

        private final int capacity;

        BuffSize(int capacity) {
            this.capacity = capacity;
        }
    }

    private static final class CachedBuffer {
        byte[] buffer;
        long lastUseTime;

        private CachedBuffer(int capacity) {
            buffer = new byte[capacity];
            lastUseTime = System.currentTimeMillis();
        }

        byte[] getBuffer() {
            lastUseTime = System.currentTimeMillis();
            return buffer;
        }
    }


    private final Reference<CachedBuffer>[] cachedBuffers = new Reference[BuffSize.values().length];

    //The period after which we remove old caches
    private final long cleanupPeriod;
    //The period after which we change big cache size references from soft to weak;
    private final long demotionPeriod;
    private long lastCleanupTime;

    public BufferCache(long cleanupPeriodInMins) {
        this.cleanupPeriod = cleanupPeriodInMins * 60 * 1000;
        this.demotionPeriod = cleanupPeriod / 2;
        lastCleanupTime = System.currentTimeMillis() + cleanupPeriod;
    }


    public byte[] getTiny() {
        return getByBuffSize(BuffSize.TINY);
    }

    public byte[] getSmall() {
        return getByBuffSize(BuffSize.SMALL);
    }

    public byte[] getMedium() {
        return getByBuffSize(BuffSize.MEDIUM);
    }

    public byte[] getBig() {
        return getByBuffSize(BuffSize.BIG);
    }

    public byte[] getHuge() {
        return getByBuffSize(BuffSize.HUGE);
    }

    public byte[] getMega() {
        return getByBuffSize(BuffSize.MEGA);
    }

    public byte[] getBuffer(int capacity) {
        if (capacity <= BuffSize.MEDIUM.capacity) {
            return getTiny();
        }
        if (capacity <= BuffSize.SMALL.capacity) {
            return getTiny();
        }
        if (capacity <= BuffSize.TINY.capacity) {
            return getTiny();
        }
        if (capacity <= BuffSize.BIG.capacity) {
            return getTiny();
        }
        if (capacity <= BuffSize.HUGE.capacity) {
            return getTiny();
        }
        if (capacity <= BuffSize.MEGA.capacity) {
            return getTiny();
        }
        throw new UnsupportedBufferSizeException("BytesCache does not support capacity " + capacity);
    }

    private byte[] getByBuffSize(BuffSize buffSize) {
        Reference<CachedBuffer> bufferReference = cachedBuffers[buffSize.ordinal()];
        CachedBuffer cachedBuffer;
        long now = System.currentTimeMillis();
        if (now - lastCleanupTime >= cleanupPeriod) {
            cleanup();
        }
        if (bufferReference != null) {
            cachedBuffer = bufferReference.get();
            if (cachedBuffer != null) {
                return cachedBuffer.getBuffer();
            }
        }
        cachedBuffer = new CachedBuffer(buffSize.capacity);
        cachedBuffers[buffSize.ordinal()] = new SoftReference<>(cachedBuffer);
        return cachedBuffer.getBuffer();
    }

    private void cleanup() {
        for(int i = 0; i < cachedBuffers.length; i++) {
            Reference<CachedBuffer> bufferReference = cachedBuffers[i];
            if (bufferReference == null) {
                continue;
            }
            CachedBuffer cachedBuffer = bufferReference.get();
            if (cachedBuffer == null) {
                continue;
            }
            if (cachedBuffer.buffer.length < BuffSize.BIG.capacity) {
                continue;
            }
            long now = System.currentTimeMillis();
            long nonUseTime = now - cachedBuffer.lastUseTime;
            if (nonUseTime >= cleanupPeriod) {
                cachedBuffers[i] = null;
            } else if (nonUseTime >= demotionPeriod) {
                // replace soft reference with weak reference
                cachedBuffers[i] = new WeakReference<>(cachedBuffer);
                bufferReference.clear();
            }
        }
    }

}
