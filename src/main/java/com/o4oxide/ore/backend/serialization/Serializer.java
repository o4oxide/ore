package com.o4oxide.ore.backend.serialization;

public interface Serializer<MESSAGE> {
    byte[] serialize(MESSAGE object);
    MESSAGE deserialize(byte[] bytes);
}
