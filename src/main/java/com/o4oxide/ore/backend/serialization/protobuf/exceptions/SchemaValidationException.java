package com.o4oxide.ore.backend.serialization.protobuf.exceptions;

import com.o4oxide.ore.exceptions.OreRuntimeException;

public class SchemaValidationException extends OreRuntimeException {
    public SchemaValidationException(String message) {
        super(message);
    }
    public SchemaValidationException(Throwable ex) {
        super(ex);
    }
}
