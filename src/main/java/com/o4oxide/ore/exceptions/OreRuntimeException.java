package com.o4oxide.ore.exceptions;

public abstract class OreRuntimeException extends RuntimeException {
    public OreRuntimeException(String message) {
        super(message);
    }
    public OreRuntimeException(String message, Throwable ex) {
        super(message, ex);
    }
    public OreRuntimeException(Throwable ex) {
        super(ex);
    }
}
