package com.o4oxide.ore;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;

import static java.util.concurrent.Flow.Subscription;
import static java.util.concurrent.Flow.Subscriber;

public interface Ore {

    static Ore create() {
        return OreImpl.create();
    }

    static Ore create(ForkJoinPool pool) {
        return OreImpl.create(pool);
    }

    long generateUniqueKey();
    CompletableFuture<Integer> createTopic(String sql);
    CompletableFuture<Integer> createMaterializedView(String sql);
    CompletableFuture<Integer> logEvent(String sql);
    <T> Subscription query(String query, Subscriber<T> subscriber);
}
