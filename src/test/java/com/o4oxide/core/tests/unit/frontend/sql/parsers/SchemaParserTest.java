package com.o4oxide.core.tests.unit.frontend.sql.parsers;

import com.o4oxide.ore.core.exceptions.FieldSchemaException;
import com.o4oxide.ore.core.models.schema.BoundableFieldSchema;
import com.o4oxide.ore.core.models.schema.EnumSchema;
import com.o4oxide.ore.core.models.schema.FieldSchema;
import com.o4oxide.ore.core.models.schema.RecordSchema;
import com.o4oxide.ore.frontend.sql.exceptions.InvalidSqlException;
import com.o4oxide.ore.frontend.sql.exceptions.UnsupportedSqlException;
import com.o4oxide.ore.frontend.sql.exceptions.UnsupportedSqlTypeException;
import com.o4oxide.ore.frontend.sql.parsers.SchemaParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

import static com.o4oxide.ore.core.models.schema.FieldSchema.Type;
import static org.junit.jupiter.api.Assertions.*;

public class SchemaParserTest {

    @Test
    public void just_test() {
        SchemaParser schemaParser = new SchemaParser();
        RecordSchema record = schemaParser.parse(
                        "CREATE TABLE record (a INT[10] NOT NULL DEFAULT 10, b VARCHAR(255), c BIGINT, d CHAR," +
                        "              e BOOL, f ENUM(ONE), h BYTEA)" +
                        "              CACHE_SIZE=5;"
        );
        assertEquals("record", record.getName());
    }

    @Test
    public void test_convert_sql_type_returns_correct_field_schema_type() {
        Assertions.assertEquals(FieldSchema.Type.INTEGER, SchemaParser.convertSqlType("INT"));
        Assertions.assertEquals(FieldSchema.Type.INTEGER, SchemaParser.convertSqlType("INTEGER"));
        Assertions.assertEquals(FieldSchema.Type.BOOLEAN, SchemaParser.convertSqlType("BIT"));
        Assertions.assertEquals(FieldSchema.Type.BOOLEAN, SchemaParser.convertSqlType("BOOL"));
        Assertions.assertEquals(FieldSchema.Type.BOOLEAN, SchemaParser.convertSqlType("BOOLEAN"));
        Assertions.assertEquals(FieldSchema.Type.LONG, SchemaParser.convertSqlType("BIGINT"));
        Assertions.assertEquals(FieldSchema.Type.LONG, SchemaParser.convertSqlType("INT8"));
        Assertions.assertEquals(FieldSchema.Type.FLOAT, SchemaParser.convertSqlType("FLOAT"));
        Assertions.assertEquals(FieldSchema.Type.FLOAT, SchemaParser.convertSqlType("REAL"));
        Assertions.assertEquals(FieldSchema.Type.DOUBLE, SchemaParser.convertSqlType("DOUBLE"));
        Assertions.assertEquals(FieldSchema.Type.DOUBLE, SchemaParser.convertSqlType("FLOAT8"));
        Assertions.assertEquals(FieldSchema.Type.BYTES, SchemaParser.convertSqlType("BYTEA"));
        Assertions.assertEquals(FieldSchema.Type.BYTES, SchemaParser.convertSqlType("BINARY"));
        Assertions.assertEquals(FieldSchema.Type.BYTES, SchemaParser.convertSqlType("RAW"));
        Assertions.assertEquals(FieldSchema.Type.STRING, SchemaParser.convertSqlType("VARCHAR"));
        Assertions.assertEquals(FieldSchema.Type.STRING, SchemaParser.convertSqlType("NVARCHAR"));
        Assertions.assertEquals(FieldSchema.Type.STRING, SchemaParser.convertSqlType("CHAR"));
        Assertions.assertEquals(FieldSchema.Type.STRING, SchemaParser.convertSqlType("NCHAR"));
        Assertions.assertEquals(FieldSchema.Type.STRING, SchemaParser.convertSqlType("CHARACTER"));
        Assertions.assertEquals(FieldSchema.Type.RECORD, SchemaParser.convertSqlType("RECORD"));
        Assertions.assertEquals(FieldSchema.Type.ENUM, SchemaParser.convertSqlType("ENUM"));
    }

    @Test
    public void test_happy_flow() {
        SchemaParser schemaParser = new SchemaParser();
        RecordSchema record = schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT, b CHAR);" +
                        "DECLARE @rec2 TABLE (rec1 RECORD, b CHAR);" +
                        "DECLARE @rec3 TABLE (rec2 RECORD, b CHAR);" +
                        "DECLARE @rec4 TABLE (a INT, b CHAR);" +
                        "DECLARE @rec5 TABLE (rec4 RECORD, b CHAR);" +
                        "DECLARE @rec6 TABLE (rec4 RECORD, b CHAR);" +
                        "DECLARE @rec7 TABLE (rec6 RECORD, b CHAR);" +
                        "DECLARE @rec8 TABLE (rec7 RECORD, b CHAR);" +
                        "CREATE  TABLE record (a INT, b VARCHAR(10,256), c BIGINT, d CHAR," +
                                "              e BOOL, f ENUM(SLOW, FAST, HAPPY)[20], g BYTEA, h BYTEA," +
                                "              rec1 RECORD, rec2 RECORD, rec3 RECORD, rec4 RECORD," +
                                "              rec5 RECORD, rec6 RECORD, rec7 RECORD, rec8 RECORD);"
                );
        assertEquals("record", record.getName());
        assertEquals(16, record.getFields().size());
        Set<FieldSchema> expectedFields = new LinkedHashSet<>() {{
            add(new FieldSchema("a", Type.INTEGER));
            add(new FieldSchema("b", Type.STRING));
            add(new FieldSchema("c", Type.LONG));
            add(new FieldSchema("d", Type.STRING));
            add(new FieldSchema("e", Type.BOOLEAN));
            add(new FieldSchema("f", Type.ENUM));
            add(new FieldSchema("g", Type.BYTES));
            add(new FieldSchema("h", Type.BYTES));
        }};
        RecordSchema rec1 = new RecordSchema("rec1").addField(new FieldSchema("a", Type.INTEGER)).addField(new FieldSchema("b", Type.STRING));
        RecordSchema rec2 = new RecordSchema("rec2").addField(rec1).addField(new FieldSchema("b", Type.STRING));
        RecordSchema rec3 = new RecordSchema("rec3").addField(rec2).addField(new FieldSchema("b", Type.STRING));
        RecordSchema rec4 = new RecordSchema("rec4").addField(new FieldSchema("a", Type.INTEGER)).addField(new FieldSchema("b", Type.STRING));
        RecordSchema rec5 = new RecordSchema("rec5").addField(rec4).addField(new FieldSchema("b", Type.STRING));
        RecordSchema rec6 = new RecordSchema("rec6").addField(rec4).addField(new FieldSchema("b", Type.STRING));
        RecordSchema rec7 = new RecordSchema("rec7").addField(rec6).addField(new FieldSchema("b", Type.STRING));
        RecordSchema rec8 = new RecordSchema("rec8").addField(rec7).addField(new FieldSchema("b", Type.STRING));
        Set<RecordSchema> expectedRecordFields = new LinkedHashSet<>() {{
            add(rec1);
            add(rec2);
            add(rec3);
            add(rec4);
            add(rec5);
            add(rec6);
            add(rec7);
            add(rec8);
        }};
        expectedFields.addAll(expectedRecordFields);
        assertRecordFields(record, expectedFields);
        FieldSchema bf = record.getFields().stream()
                .filter(fld -> fld.getName().equals("b"))
                .findFirst()
                .orElseThrow();
        BoundableFieldSchema bField = (BoundableFieldSchema) bf;
        Assertions.assertEquals(10, bField.getMinimum());
        Assertions.assertEquals(256, bField.getMaximum());

        FieldSchema ff = record.getFields().stream()
                .filter(fld -> fld.getName().equals("f"))
                .findFirst()
                .orElseThrow();
        EnumSchema fField = (EnumSchema) ff;
        Assertions.assertEquals(3, fField.getValues().size());
        Iterator<String> enumValIterator = fField.getValues().iterator();
        enumValIterator.next();//skip the first one
        Assertions.assertEquals("FAST", enumValIterator.next());
        Assertions.assertEquals(20, fField.getArraySize());
    }

    private void assertRecordFields(RecordSchema record, Set<FieldSchema> expectedFields) {
        Iterator<FieldSchema> fieldsIterator = record.getFields().iterator();
        for (FieldSchema expectedField : expectedFields) {
            assertTrue(fieldsIterator.hasNext());
            FieldSchema field = fieldsIterator.next();
            assertEquals(expectedField.getName(), field.getName());
            assertEquals(expectedField.getType(), field.getType());
            if (field.getType() == Type.RECORD) {
                RecordSchema recordField = (RecordSchema) field;
                RecordSchema expectedRecordField = (RecordSchema) expectedField;
                assertRecordFields(recordField, expectedRecordField.getFields());
            }
            if (field.getType() == Type.ENUM) {
                EnumSchema enumField = (EnumSchema) field;
                Assertions.assertFalse(enumField.getValues().isEmpty());
            }
        }
        assertFalse(fieldsIterator.hasNext());
    }

    @Test
    public void test_declare_as_throws_unsupported_sql_exception() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(UnsupportedSqlException.class,
                ()-> schemaParser.parse("DECLARE @table AS VARCHAR"));
        assertEquals("DECLARE is only allowed on TABLE type", ex.getMessage());
        ex = assertThrows(UnsupportedSqlException.class,
                ()-> schemaParser.parse("DECLARE @table AS TABLE"));
        assertEquals("DECLARE is only allowed on TABLE type", ex.getMessage());
    }

    @Test
    public void test_unknown_data_types_throw_unsupported_sql_exception() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(UnsupportedSqlTypeException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec TABLE (a ABRCADABRA);" +
                        "CREATE TABLE table (rec RECORD, a INT);"
                ));
        assertEquals("ABRCADABRA is not supported", ex.getMessage());
    }

    @Test
    public void test_max_nesting_depth_is_not_more_than_MAX_NESTING_DEPTH() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(FieldSchemaException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT, b CHAR);" +
                        "DECLARE @rec2 TABLE (rec1 RECORD, b CHAR);" +
                        "DECLARE @rec3 TABLE (rec2 RECORD, b CHAR);" +
                        "DECLARE @rec4 TABLE (a INT, b CHAR);" +
                        "DECLARE @rec5 TABLE (rec4 RECORD, b CHAR);" +
                        "DECLARE @rec6 TABLE (rec5 RECORD, b CHAR);" +
                        "DECLARE @rec7 TABLE (rec6 RECORD, b CHAR);" +
                        "DECLARE @rec8 TABLE (rec7 RECORD, b CHAR);" +
                        "CREATE  TABLE record (rec1 RECORD, rec2 RECORD, rec3 RECORD," +
                        "                      rec4 RECORD, rec5 RECORD, rec6 RECORD," +
                        "                      rec7 RECORD, rec8 RECORD);"
                ));
        assertEquals("Exceeded permitted nesting depth (4)", ex.getMessage());
    }

    @Test
    public void test_fields_in_declare_statements_are_not_more_than_MAX_FIELDS_PER_RECORD() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(FieldSchemaException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT,    b CHAR,    c INT,   d VARCHAR, " +
                            "                     e BIGINT, f BOOL,    g INT,   h VARCHAR, " +
                            "                     i CHAR,   j VARCHAR, k FLOAT, l DOUBLE,  " +
                            "                     m NCHAR,  n VARCHAR, o CHAR,  p FLOAT,    " +
                            "                     q BOOL                                ); " +
                        "CREATE TABLE table (c INT, rec1 RECORD);"
                ));
        assertEquals("Exceeded permitted number of fields (16)", ex.getMessage());
    }

    @Test
    public void test_declare_statements_are_not_more_than_MAX_FIELDS_PER_RECORD() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(FieldSchemaException.class,
                ()-> schemaParser.parse(
                            "DECLARE @rec1 TABLE (a INT, b CHAR); DECLARE @rec2 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec3 TABLE (a INT, b CHAR); DECLARE @rec4 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec5 TABLE (a INT, b CHAR); DECLARE @rec6 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec7 TABLE (a INT, b CHAR); DECLARE @rec8 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec9 TABLE (a INT, b CHAR); DECLARE @rec10 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec11 TABLE (a INT, b CHAR); DECLARE @rec12 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec13 TABLE (a INT, b CHAR); DECLARE @rec14 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec15 TABLE (a INT, b CHAR); DECLARE @rec16 TABLE (a INT, b CHAR);" +
                                "DECLARE @rec17 TABLE (a INT, b CHAR);" +
                                "CREATE  TABLE record (rec1  RECORD, rec2  RECORD, rec3  RECORD, rec4  RECORD," +
                                "                      rec5  RECORD, rec6  RECORD, rec7  RECORD, rec8  RECORD," +
                                "                      rec9  RECORD, rec10 RECORD, rec11 RECORD, rec12 RECORD," +
                                "                      rec13 RECORD, rec14 RECORD, rec15 RECORD, rec16 RECORD," +
                                "                      rec17 RECORD);"
                ));
        assertEquals("Exceeded permitted number of fields (16)", ex.getMessage());
    }

    @Test
    public void test_fields_in_create_statement_is_not_more_than_MAX_FIELDS_PER_RECORD() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(FieldSchemaException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT, b CHAR);" +
                        "CREATE TABLE table (rec1 RECORD, a INT, b CHAR, c INT, d VARCHAR, e BIGINT, f BOOL," +
                        "                    g INT, h VARCHAR, i BIGINT, j BIGINT, k FLOAT, l DOUBLE, m INT," +
                        "                    n VARCHAR, o CHAR, p INT, q CHAR);"
                ));
        assertEquals("Exceeded permitted number of fields (16)", ex.getMessage());
    }

    @Test
    public void test_field_name_is_not_more_than_MAX_LENGTH_OF_FIELD_NAME() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(FieldSchemaException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (aaaaveryveryveryveryveryvlonglonglonglonglonglonglongnamenamename INT);" +
                        "CREATE TABLE table (rec1 RECORD, a INT, b CHAR);"
                ));
        assertEquals("Exceeded permitted length for field names (64)", ex.getMessage());
    }

    @Test
    public void test_field_names_are_unique_per_record() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(FieldSchemaException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT, b VARCHAR);" +
                        "CREATE TABLE table (rec1 RECORD, a INT, a CHAR);"
                ));
        assertEquals("Field name is already used (a)", ex.getMessage());
    }

    @Test
    public void test_record_name_is_not_more_than_MAX_LENGTH_OF_RECORD_NAME() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(FieldSchemaException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT);" +
                        "CREATE TABLE aaaaveryveryveryveryveryvlonglonglonglonglonglonglongnamenamename (rec1 RECORD, a INT, b CHAR);"
                ));
        assertEquals("Exceeded permitted length for field names (64)", ex.getMessage());
    }

    @Test
    public void test_record_variables_must_be_declared_before_being_referenced() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(InvalidSqlException.class,
                ()-> schemaParser.parse(
                        "CREATE TABLE table (rec1 RECORD, a INT, b CHAR, c INT);"
                ));
        assertEquals("Cannot find declaration for record variable rec1", ex.getMessage());
    }

    @Test
    public void test_all_declared_record_variables_must_be_used() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(InvalidSqlException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT, b CHAR, c INT);" +
                        "DECLARE @rec2 TABLE (a INT, b CHAR, c INT);" +
                        "CREATE TABLE table (c INT, rec1 RECORD);"
                ));
        assertEquals("Some declared record variables were not used", ex.getMessage());
    }

    @Test
    public void test_create_statement_must_be_the_last_in_schema_definition() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(InvalidSqlException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT, b CHAR, c INT);" +
                        "DECLARE @rec2 TABLE (a INT, b CHAR, c INT);" +
                        "CREATE TABLE table (c INT, rec1 RECORD, rec2 RECORD);"    +
                        "DECLARE @rec3 TABLE (a INT, b CHAR, c INT);"
                ));
        assertEquals("Schema definition should end with single CREATE statement", ex.getMessage());
        ex = assertThrows(InvalidSqlException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT, b CHAR, c INT);" +
                            "DECLARE @rec2 TABLE (a INT, b CHAR, c INT);" +
                            "CREATE TABLE table (c INT, rec1 RECORD, rec2 RECORD);"    +
                            "CREATE TABLE table2 (a INT, b CHAR, c INT);"
                ));
        assertEquals("Schema definition should end with single CREATE statement", ex.getMessage());
    }

    @Test
    public void test_arguments_string_is_only_allowed_on_enum_and_boundable_types() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(InvalidSqlException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (a INT);" +
                            "CREATE TABLE table (rec1 RECORD, a INT(1024), b ENUM(HAPPY, SAD));"
                ));
        assertEquals("Field type is not boundable (a)", ex.getMessage());
    }

    @Test
    public void test_enum_type_requires_definition() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(InvalidSqlException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (enuma ENUM);" +
                            "CREATE TABLE table (rec1 RECORD, a INT, b ENUM(HAPPY, SAD));"
                ));
        assertEquals("Enum type requires value specification (enuma)", ex.getMessage());
    }

    @Test
    public void test_boundable_type_definition_must_be_an_integer() {
        SchemaParser schemaParser = new SchemaParser();
        Throwable ex = assertThrows(InvalidSqlException.class,
                ()-> schemaParser.parse(
                        "DECLARE @rec1 TABLE (str VARCHAR('HAPPY', 'SAD'));" +
                            "CREATE TABLE table (rec1 RECORD, a INT, b VARCHAR(5));"
                ));
        assertEquals("Boundable type argument must be integer (str)", ex.getMessage());
    }
}
