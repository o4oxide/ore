package com.o4oxide.core.tests.unit;

import com.o4oxide.ore.Ore;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class OreTest {

    @Test
    public void test_unique_key_generation() throws InterruptedException {
        Map<String, Long> keyMap = new HashMap<>();
        Thread keyOneThread = new Thread(()-> keyMap.put("key1", Ore.create().generateUniqueKey()));
        Thread keyTwoThread = new Thread(()-> keyMap.put("key2", Ore.create().generateUniqueKey()));
        keyTwoThread.start();
        keyOneThread.start();

        keyOneThread.join();
        keyTwoThread.join();

        assertNotNull(keyMap.get("key1"));
        assertNotNull(keyMap.get("key2"));
        assertTrue(keyMap.get("key1") > 0);
        assertTrue(keyMap.get("key2") > 0);
        assertNotEquals(keyMap.get("key1"), keyMap.get("key2"));
    }


}
