package com.o4oxide.core.tests.unit.backend.serialization.protobuf;


import com.o4oxide.ore.backend.serialization.protobuf.ProtobufSerializer;
import com.o4oxide.ore.core.models.schema.RecordSchema;

import org.junit.jupiter.api.Test;

public class ProtobufSerializerTest {

    @Test
    public void testBuildSchema() {
        ProtobufSerializer serializer = new ProtobufSerializer();
        byte[] schema = serializer.buildSchema(buildTestSchema());
        System.out.println("schema = " + new String(schema));
    }

    private RecordSchema buildTestSchema() {
        /*SchemaParser schemaParser = new SchemaParser();
        return schemaParser.parse(
                    "DECLARE @rec1 TABLE (a INT, b CHAR);" +
                        "DECLARE @rec2 TABLE (rec1 RECORD, b CHAR);" +
                        "DECLARE @rec3 TABLE (rec2 RECORD, b CHAR);" +
                        "DECLARE @rec4 TABLE (a INT, b CHAR);" +
                        "DECLARE @rec5 TABLE (rec4 RECORD, b CHAR);" +
                        "DECLARE @rec6 TABLE (rec4 RECORD, b CHAR);" +
                        "DECLARE @rec7 TABLE (rec6 RECORD, b CHAR);" +
                        "DECLARE @rec8 TABLE (rec7 RECORD, b CHAR);" +
                        "CREATE  TABLE record (a INT, b VARCHAR, c BIGINT, d CHAR," +
                        "              e BOOL, f ENUM, g ARRAYS, h BYTEA," +
                        "              rec1 RECORD, rec2 RECORD, rec3 RECORD, rec4 RECORD," +
                        "              rec5 RECORD, rec6 RECORD, rec7 RECORD, rec8 RECORD);"
        );
        */
        return null;
    }
}

