package com.o4oxide.core.tests.unit.utils;


import com.o4oxide.ore.utils.Checkers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CheckersTest {

    @Test
    public void testIsNumeric() {
        assertTrue(Checkers.isNumeric("123"));
        assertFalse(Checkers.isNumeric("abc"));
        assertFalse(Checkers.isNumeric("123abc"));
        assertFalse(Checkers.isNumeric("123.45"));
    }
}
