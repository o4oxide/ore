package com.o4oxide.core.tests.integration;

import com.o4oxide.ore.Ore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class OreSchemaIntTest {

    private static Ore ore;

    @BeforeAll
    public static void setup() {
        ore = Ore.create();
    }

    @Test
    public void testCreateTable() {
        ore.createTopic(
                "DECLARE @address TABLE (       " +
                "       street VARCHAR,         " +
                "       city VARCHAR,           " +
                "       state VARCHAR           " +
                "       postal_code VARCHAR     " +
                ");                             " +
                "DECLARE @preferences TABLE (   " +
                "       key VARCHAR,            " +
                "       value VARCHAR,          " +
                ");                             " +
                "CREATE TABLE customer (        " +
                "       customer_id INT,        " +
                "       first_name VARCHAR,     " +
                "       last_name VARCHAR,      " +
                "       address TABLE,          " +
                "       preferences TABLE,      " +
                "       created_date TIMESTAMP  " +
                ");"
        );
    }
}
